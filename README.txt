About Metro Theme
====================
Metro is a fixed width (980px) theme. The theme is not dependent on any 
core theme. Its very light weight for fast loading with modern look.
  Simple and clean design
  Fixed width (980px)
  Drupal standards compliant
  Custom front-page with 4 block regions
  Implementation of a JS Slideshow
  Multi-level drop-down menus
  Use of Google Web Fonts
  Custom front-page with 4 block regions
  Footer with 4 regions
  A total of 12 regions
  Compatible and tested on Modern browsers

Browser compatibility:
=====================
The theme has been tested on following browsersn mentioned below.
IE8+, Firefox, Google Chrome, Opera.

Drupal compatibility:
=====================
This theme is compatible with Drupal 7.x.x

Developed by
============
http://www.ezeegeeks.com/

jQuery(function($){
    $(document).ready(function(){
      // superFish
       $('#main-menu ul.menu').supersubs({
       // minimum width of sub-menus in em units
       minWidth:    16,
       // maximum width of sub-menus in em units
       maxWidth:    40,
       // extra width can ensure lines don't sometimes turn over
       extraWidth:  1
     })
    // call supersubs first, then superfish
    .superfish();
     });
});

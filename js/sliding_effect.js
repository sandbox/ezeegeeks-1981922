jQuery(document).ready(function($) {

  //Set Default State of each portfolio piece
  $(".paging").show();
  $(".paging a:first").addClass("active");

  //Get size of images, how many there are, then determin the size of the image reel.
  var imageWidth = $(".window").width();
  var imageSum = $(".image_reel img").size();
  var imageReelWidth = imageWidth * imageSum;

  //Adjust the image reel to its new size
  $(".image_reel").css({'width' : imageReelWidth});

  //Paging + Slider Function
  rotate = function(){
      //Get number of times to slide
      var triggerID = $active.attr("rel") - 1;
      //Determines the distance the image reel needs to slide
      var image_reelPosition = triggerID * imageWidth;
      //Remove all active class
      $(".paging a").removeClass('active');
      //Add active class (the $active is declared in the rotateSwitch function)
      $active.addClass('active');

    $(".desc").stop(true,true).slideUp('slow');

    $(".desc").eq($('.paging a.active').attr("rel") - 1).slideDown("slow");

      //Slider Animation
      $(".image_reel").animate({
          left: -image_reelPosition
      }, 500);
  };

  //Rotation + Timing Event
  rotateSwitch = function(){
  $(".desc").eq($('.paging a.active').attr("rel") - 1).slideDown("slow");
      //Set timer - this will repeat itself every 3 seconds
      play = setInterval(function(){
          $active = $('.paging a.active').next();
          //If paging reaches the end...
          if ($active.length === 0) {
              //go back to first
              $active = $('.paging a:first');
          }
          //Trigger the paging and slider function
          rotate();
      //Timer speed in milliseconds (3 seconds)
      }, 10000);
  };
  //Run function on launch
  rotateSwitch();

 //On Click
    $(".paging a").click(function() {
        //Activate the clicked paging
        $active = $(this);
        //Reset Timer
        //Stop the rotation
        clearInterval(play);
        //Trigger rotation immediately
        rotate();
        // Resume rotation
        rotateSwitch();
        //Prevent browser jump to link anchor
        return false;
    });
});
